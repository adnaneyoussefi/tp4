import React, { useEffect} from 'react';
import { Badge, Card, CardBody, CardImg, CardSubtitle } from 'reactstrap';
import Pagine from './Pagine';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useSelector, useDispatch } from 'react-redux';
import { getRepos, setPage } from '../../actions/repos-action';

const Repos = () => {
    const repos = useSelector(state => state.repos);
    const { items, loading, currentPage } = repos;
    const dispatch = useDispatch();
    useEffect(() => {
            dispatch(getRepos(currentPage));
    }, [currentPage])

    const modifierPageCourante = (e) => {
        dispatch(setPage(e.selected + 1));
    }

    return (
        <div>
            <Pagine modifierPageCourante={modifierPageCourante} currentPage={currentPage} />
            {loading ? <CircularProgress /> :
            items.map(d => {
                return(
                    <Card key={d.id}>
                        <CardBody>
                            <CardImg className={"mr-5"} style={{height: "180px",width: "180px"}}  width="180px" src={d.owner.avatar_url} />
                                <div className={"d-inline-block"}>
                                    <CardSubtitle ><b>{d.name}</b></CardSubtitle>
                                    <CardSubtitle >{d.description}</CardSubtitle>
                                    <Badge color="warning" className={"mr-2"}>Stars: {d.stargazers_count}</Badge>
                                    <Badge color="info" >Open issues: {d.open_issues_count}</Badge>   
                                </div>                     
                        </CardBody>
                    </Card>
                )
            })}
            <Pagine modifierPageCourante={modifierPageCourante} currentPage={currentPage} />
        </div>
    )
}

export default Repos;
