import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import { editTodo } from '../../actions/todo-action';
import { useDispatch } from 'react-redux';

const EditTodo = ({ id, value }) => {
  const dispatch = useDispatch();  
  const [open, setOpen] = React.useState(false);
  const [val, setVal] = React.useState(value);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
        <IconButton onClick={handleClickOpen}>
            <EditIcon />
        </IconButton>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Editer Todo</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Todo"
            value={val}
            onChange={(e) => setVal(e.currentTarget.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Annuler
          </Button>
          <Button onClick={() => {dispatch(editTodo(id, val)); setOpen(false)}} color="primary">
            Editer
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default EditTodo;
