import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { useDispatch, useSelector} from 'react-redux';
import { checkTodo, removeTodo } from '../../actions/todo-action';
import EditTodo from './EditTodo';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  input: {
    backgroundColor: '#EDE9FF'  }
}));

const ListTodos = () => {
  const todos = useSelector(state => state.todos.todos);
  const dispatch = useDispatch();
  const classes = useStyles();

  return (
    <>
    {todos.length ? <List className={classes.root}>
      {todos && todos.map(({id, value, done}) => {
        const labelId = `checkbox-list-label-${id}`;

        return (
          <ListItem className={classes.input} key={id} role={undefined} dense button onClick={() => dispatch(checkTodo(id))}>
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={done}
                tabIndex={-1}
                disableRipple
                inputProps={{ 'aria-labelledby': labelId }}
              />
            </ListItemIcon>
            <ListItemText id={labelId} primary={value} />
            <ListItemSecondaryAction>
              <EditTodo value={value} id={id} />
              <IconButton edge="end" aria-label="remove" onClick={() => dispatch(removeTodo(id))}>
                <HighlightOffIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        );
      })}
    </List> : ''}
    </>
  );
}

export default ListTodos;