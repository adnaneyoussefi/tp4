import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { IconButton } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { addTodo } from '../../actions/todo-action';
import ListTodos from './ListTodos';
import { Container, Row, Col } from 'reactstrap';

const Todo = () => {
    const dispatch = useDispatch();
    const [val, setVal] = useState();

const useStyles = makeStyles(() => ({
    input: {
        borderRadius: 4,
        width: '800px'
    }
    }));
const classes = useStyles();

return (
    <>
    <div className="App">
        <Container className="pt-5">
            <Row className="d-flex justify-content-center">
                <Col xs="auto">
                    <TextField value={val} 
                    onChange={(e) => setVal(e.currentTarget.value)} 
                    className={classes.input} id="filled-basic" 
                    label="Ajouter un Todo" 
                    variant="filled" 
                    color="secondary" />
                </Col>
                <Col xs="auto">
                    <IconButton onClick={() => {dispatch(addTodo(val)); setVal("")}}>
                        <AddCircleIcon color="secondary" fontSize="large" />
                    </IconButton>
                </Col>
            </Row>
            <Row className="d-flex justify-content-center">
                <ListTodos />
            </Row>
        
            
        </Container>    
    </div>
    </>
)
}

export default Todo;
