import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useSelector, useDispatch } from 'react-redux';
import { signIn } from '../actions/user-action';

const Login = ({ history }) => {
    const user = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const [nom, setNom] = useState('');
    const [classe, setClasse] = useState('');

    useEffect(() => {
        console.log(user.isAuth)
        if(user.isAuth)
            history.push('/tp1');
    }, [user])
    return (
        <Container>
            <Row style={{height: '100vh'}} className="d-flex flex-column align-items-center justify-content-center">
                <Col xs="auto" className="mb-4">
                    <TextField onChange={(e) => setNom(e.currentTarget.value)} id="outlined-basic" label="Utilisateur" variant="outlined" />    
                </Col>
                <Col xs="auto" className="mb-4">
                    <TextField onChange={(e) => setClasse(e.currentTarget.value)} id="outlined-basic" label="Nom de la classe" variant="outlined" />
                </Col>
                <Col xs="auto">
                    <Button onClick={() => dispatch(signIn(nom, classe))} variant="contained" color="secondary">
                        LOGIN
                    </Button>
                </Col>
            </Row>
        </Container>
    )
}

export default Login;
