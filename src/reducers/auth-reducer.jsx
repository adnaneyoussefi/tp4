import { AUTH_SUCCESS, AUTH_FAILED, USER_LOGOUT } from '../constants';

const initialState = {
    isAuth: false,
    user: '',
    classe: '',
    error: null
}

const authReducer = (state = initialState, action) => {
    switch(action.type){
        case AUTH_SUCCESS:
            return {
                ...state,
                isAuth: true,
                user: action.payload.user,
                classe: action.payload.classe
            };
        case AUTH_FAILED:
            return {
                ...state,
                isAuth: false,
                error: action.payload
            };  
        case USER_LOGOUT:
        return {
            ...state,
            isAuth: false
        };      
        default:
            return state;
    }
}
export default authReducer;