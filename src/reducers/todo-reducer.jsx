import { GET_TODOS, ADD_TODOS, DELETE_TODO, CHECK_TODO, EDIT_TODO } from '../constants';

const initialState = {
    todos: []
}

const todoReducer = (state = initialState, action) => {
    switch(action.type){
        case GET_TODOS:
            return {
                ...state,
            };
        case ADD_TODOS:
            return {
                ...state,
                todos: [...state.todos, {id: Date.now(), value: action.payload, done: false}] 
            };    
        case EDIT_TODO:
            const listTodos = state.todos.map(t => {
                if(t.id === action.payload.id)
                    t.value = action.payload.value;
                return t
            })  
            return {
                ...state,
                todos: listTodos
            };        
        case DELETE_TODO:
            let updatedList = state.todos.filter(t => t.id !== action.payload)
        return {
            ...state,
            todos: updatedList 
        };    
        case CHECK_TODO:
            const list = state.todos.map(t => {
                if(t.id === action.payload)
                    t.done = !t.done;
                return t
            })
        return {
            ...state,
            todos: list
        };    
        default:
            return state;
    }
}
export default todoReducer;