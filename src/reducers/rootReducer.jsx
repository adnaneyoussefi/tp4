import { combineReducers } from "redux";
import todoReducer from './todo-reducer';
import reposReducer from './repos-reducer';
import authReducer from './auth-reducer';

const rootReducer = combineReducers({
    auth: authReducer,
    todos: todoReducer,
    repos: reposReducer
})

export default rootReducer;