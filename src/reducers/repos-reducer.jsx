import { GET_REPOS, SET_PAGE } from '../constants';

const initialState = {
    items: [],
    loading: true,
    currentPage: 1
}

const reposReducer = (state = initialState, action) => {
    switch(action.type){
        case GET_REPOS:
            return {
                ...state,
                items: action.payload,
                loading: false
            };
        case SET_PAGE:
        return {
            ...state,
            loading: true,
            currentPage: action.payload
        };
        default:
            return state;
    }
}
export default reposReducer;