import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';

const ProtectedRoutes = ({ component: Component, ...rest }) => {
    const user = useSelector(state => state.auth);
    return (
        <>
            <Route
            {...rest}
            render = { props => user.isAuth ? <Component {...props}/> : <Redirect to='/' /> }
            />
        </>
    )
}

export default ProtectedRoutes;
