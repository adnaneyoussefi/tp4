import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import configureStore from './store/configureStore';
import { Provider } from "react-redux";
import { createBrowserHistory } from "history";
import { onLoadingSignIn } from './actions/user-action';

export const history = createBrowserHistory({forceRefresh: true});
configureStore.dispatch(onLoadingSignIn());
ReactDOM.render(
  <React.StrictMode>
    <Provider store = { configureStore }>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
