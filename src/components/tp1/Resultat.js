const Resultat = ({ res, status}) => {
    let color = ""
    status == "poids normal" ? color = "green" : status == "surpoids" ? color = "yellow"
    : color = "red"
    return <div className="card">
        {res}
        <div>
            <span style={{ color: color }}>{ status }</span>
        </div>
    </div>
}
export default Resultat;