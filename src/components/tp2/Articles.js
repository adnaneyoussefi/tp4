import React, { useState } from 'react'
import AjoutPost from './AjoutPost'
import { data } from './data'
import { Card, CardText, CardBody, CardTitle, Input, Button } from 'reactstrap'
import IconButton from '@material-ui/core/IconButton'
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt'  
import ThumbDownAltIcon from '@material-ui/icons/ThumbDownAlt'
import DeleteIcon from '@material-ui/icons/Delete'

const Articles = () => {
    const [articles, setArticles] = useState(data)
    const [comment, setComment] = useState({
    })

    const handleLikePost = (id, event) => {
        let arr = articles.map(a => {
            if(a.id === id){
                if(event.currentTarget.getAttribute("name") === "like")
                    a.likes +=1
                else
                    a.dislikes +=1    
            }
            return a
        })
        setArticles(arr)
    } 
    const handleLikeComment = (id, idArticle, event) => {
        let arr = articles.map(a => {
            a.comments.forEach(c => {
                if(c.id === id && a.id === idArticle){
                    if(event.currentTarget.getAttribute("name") === "like")
                        c.likes += 1
                    else
                        c.dislikes += 1    
                }
            })
            return a
        }
        )
        setArticles(arr)
    } 
    const AjouterPost = (p) => {
        let id = Math.max(...articles.map(a => a.id))
        p.id = id + 1
        setArticles([p, ...articles])
    }

    const handleChange = (e) => {
        setComment({...comment, [e.target.name] : e.target.value})
    }

    const ajoutComment = (id) => {
        let maxId = Math.max(...articles.filter(a => a.id == id)[0].comments.map(c => c.id))
        comment.id = Number.isFinite(maxId) ? maxId + 1 : 1
        comment.likes = 0
        comment.dislikes = 0
        let arr = articles.map(article => {
            if(article.id == id) {
                article.comments.push(comment)
            }
            return article
        })
        setArticles(arr)
        console.log(arr)
    }

    const supprArticle = (id) => {
        let arr = articles.filter(a => a.id != id)
        setArticles(arr)
    }

    const supprComment = (idArticle, idComment) => {
        let arr = articles.map(a => {
            if(a.id == idArticle){
                let tab = a.comments.filter(c => c.id != idComment)
                a.comments = tab
            }
            return a
        })
        setArticles(arr)
    }

    return (
        <div>
            <AjoutPost ajouterPost={AjouterPost} />
            { articles && articles.map(article => {
                return (
                <Card key={article.id}>
                    <CardBody>
                    <IconButton onClick={() => supprArticle(article.id)} className={"btn btn-danger float-right"}>
                        <DeleteIcon color="secondary" />
                    </IconButton>
                        <CardTitle tag="h5">{article.user}</CardTitle>
                            <div dangerouslySetInnerHTML={{__html: article.content}}/>
                        {article.likes}
                        <IconButton name={"like"} onClick={(event) => handleLikePost(article.id, event)}>
                            <ThumbUpAltIcon style={{color:"#0089A5"}} />
                        </IconButton>
                        <IconButton name={"dislike"} onClick={(event) => handleLikePost(article.id, event)}>
                            <ThumbDownAltIcon style={{color:"#F71B5E"}} />
                        </IconButton>
                        {article.dislikes}
                    </CardBody>                  
                    <CardBody>
                    <h3>Commentaires</h3>
                    {article.comments && article.comments.map(comment => {
                    return(
                        <Card key={comment.id}> 
                            <CardBody>
                                <IconButton onClick={() => supprComment(article.id, comment.id)} className={"btn btn-danger float-right"}>
                                    <DeleteIcon color="secondary" />
                                </IconButton>
                                <CardTitle>{comment.user}</CardTitle>
                                <CardText>{comment.content}</CardText>
                                {comment.likes}
                                <IconButton name={"like"} onClick={(event) => handleLikeComment(comment.id, article.id, event)}>
                                    <ThumbUpAltIcon style={{color:"#0089A5"}}/>
                                </IconButton>
                                {comment.dislikes}
                                <IconButton name={"dislike"} onClick={(event) => handleLikeComment(comment.id, article.id, event)}>
                                    <ThumbDownAltIcon style={{color:"#F71B5E"}}/>
                                </IconButton>
                            </CardBody> 
                        </Card>
                    )
                    })}
                    </CardBody>
                    <CardBody>
                        <Input type="text" name="user" placeholder="Utilisateur" onChange={handleChange} />
                        <Input type="textarea" name="content" onChange={handleChange} />
                        <Button onClick={() => ajoutComment(article.id)} className={"btn btn-info"}>Ajouter ce commentaire</Button>
                    </CardBody>
                </Card>  
                )              
            }) }
        </div>
    )
}
export default Articles
