import React, {useState} from 'react'
import { Button, Form, FormGroup, Input, Card, CardBody } from 'reactstrap';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton'


const AjoutPost = (props) => {
    const [post, setPost] = useState({
        id: 0,
        user: '',
        content: '',
        likes: 0,
        dislikes: 0,
        comments: [
        ]
    })

    const handleChange = (e) => {
        setPost({...post, [e.target.name] : e.target.value})
    }
    return (
        <Card>
            <CardBody>
                <Form>
                    <FormGroup>
                        <Input type="text" name="user" placeholder="Utilisateur" onChange={handleChange} />
                        <Input type="textarea" name="content" onChange={handleChange} />
                    </FormGroup>
                    <FormGroup className="float-right">
                        <IconButton onClick={() => props.ajouterPost(post)} className={"btn btn-info"}>
                            <AddCircleIcon color="primary" fontSize="large" />
                        </IconButton>
                    </FormGroup>
                    
                </Form>
            </CardBody>
        </Card>
    )
}

export default AjoutPost
