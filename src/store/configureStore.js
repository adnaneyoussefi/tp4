import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import rootReducer from '../reducers/rootReducer';

const middlewares = [thunk, logger];

const configureStore = createStore(rootReducer, applyMiddleware(...middlewares));

export default configureStore;