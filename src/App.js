import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Dashboard from './Dashboard';
import Calculate from './components/tp1';
import Articles from './components/tp2/Articles';
import Repos from './containers/tp3/Repos.jsx';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Todo from './containers/tp5/Todo';
import Login from './containers/Login';
import Button from '@material-ui/core/Button';
import ProtectedRoutes from './config/ProtectedRoutes';
import { logUserOut } from './actions/user-action';
import { useSelector, useDispatch } from 'react-redux';

function App() {
  const user = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const NOM = 'nom';
  const CLASSE = 'classe';
  const nom = localStorage.getItem(NOM);
  const classe = localStorage.getItem(CLASSE);
  return (
    <BrowserRouter>
    {user.isAuth ? 
      <div className="Userbar">
        <Button onClick={() => dispatch(logUserOut())} variant="outlined" color="secondary">
              DECONNEXION
        </Button>
        <AccountCircleIcon color="action" style={{ fontSize: 70 }}/>
        <div>
          <h5>{nom}</h5>
          <span>{classe}</span>
        </div>
      </div> : ''}
      <div className="App">
      {user.isAuth ? <Dashboard /> : ''}
        <div className="container mt-4">
          <Switch>
            <Route path={"/"} component={Login} exact/>
            <ProtectedRoutes path={"/tp1"} component={Calculate} exact/>
            <ProtectedRoutes path={"/tp2"} component={Articles} exact/>
            <ProtectedRoutes path={"/tp3"} component={Repos} exact/>
            <ProtectedRoutes path={"/tp5"} component={Todo} exact/>       
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
