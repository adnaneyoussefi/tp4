import { GET_REPOS, SET_PAGE } from '../constants';
import API from '../config/axios';

export const getRepos = (currentPage) => {
    return async dispatch => {
        const res = await API.get(`search/repositories?q=created:>2020-11-07&sort=stars
        &order=desc&page=${currentPage}`);
        dispatch({
            type: GET_REPOS,
            payload: res.data.items
        });
    };
}

export const setPage = (nbr) => {
    return async dispatch => {
        dispatch({
            type: SET_PAGE,
            payload: nbr
        });
    };
}