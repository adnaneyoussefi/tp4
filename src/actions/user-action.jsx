import { AUTH_SUCCESS, AUTH_FAILED, USER_LOGOUT } from '../constants';
import { history } from '../index';

const NOM = 'nom';
const CLASSE = 'classe';

export const signIn = (user, classe) => {
    return dispatch => {
        dispatch(success(user, classe));
    };
}

export const onLoadingSignIn = () => {
    return dispatch => {
        try {
            const nom = localStorage.getItem(NOM);
            const classe = localStorage.getItem(CLASSE);
            if(nom === null || classe === null)
                return dispatch(error('Vous n\'etes pas connecté!'));
            dispatch(success(nom, classe)); 
        }catch(e) {
            console.error(e)
        }
    }
}

export const logUserOut = () => {
    return dispatch => {
        try {
            localStorage.clear();
            dispatch({ type: USER_LOGOUT });
        }catch(e) {
            console.log(e);
        }
    }
}

const success = (user, classe) => {
    localStorage.setItem(NOM, user);
    localStorage.setItem(CLASSE, classe);
    return {
        type: AUTH_SUCCESS, type: AUTH_SUCCESS,
        payload: {user, classe}
    }
};

const error = (error) => {
    return {type: AUTH_FAILED, payload: error}
};