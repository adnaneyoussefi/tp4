import { GET_TODOS, ADD_TODOS, DELETE_TODO, CHECK_TODO, EDIT_TODO } from '../constants';

export const getTodos = () => {
    return dispatch => {
        
        dispatch({
            type: GET_TODOS,
        });
    };
}

export const addTodo = (val) => {
    return dispatch => {
        dispatch({
            type: ADD_TODOS,
            payload: val
        });
    };
}

export const editTodo = (id, value) => {
    return dispatch => {
        dispatch({
            type: EDIT_TODO,
            payload: {id, value}
        });
    };
}

export const removeTodo = (id) => {
    return dispatch => {
        dispatch({
            type: DELETE_TODO,
            payload: id
        });
    };
}

export const checkTodo = (id) => {
    return dispatch => {
        dispatch({
            type: CHECK_TODO,
            payload: id
        });
    };
}